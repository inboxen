##
#    Copyright (C) 2018 Jessica Tallon & Matt Molyneaux
#
#    This file is part of Inboxen.
#
#    Inboxen is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Inboxen is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with Inboxen.  If not, see <http://www.gnu.org/licenses/>.
##

from django import urls

from inboxen.account.backends import RateLimitWithSettings
from inboxen.test import InboxenTestCase, MockRequest
from inboxen.tests import factories


class LoginTestCase(InboxenTestCase):
    def test_missing_mgmt_data(self):
        good_data = {
            "auth-username": "user1",
            "auth-password": "pass1",
            "login_view-current_step": "auth",
        }

        response = self.client.post(urls.reverse("user-login"), good_data)
        # form was validated and *form* errors returned
        self.assertEqual(response.status_code, 200)

        bad_data = {
            "auth-username": "user1",
            "auth-password": "pass1",
        }
        response = self.client.post(urls.reverse("user-login"), bad_data)
        # Bad request, but no exception generated
        self.assertEqual(response.status_code, 400)

    def test_get_user_select_related(self):
        user = factories.UserFactory()
        login = self.client.login(username=user.username, password="123456", request=MockRequest(user))
        if not login:
            raise Exception("Could not log in")

        # 1 query to fetch user, 1 to fetch session
        with self.assertNumQueries(2):
            response = self.client.get(urls.reverse("user-username"))
            response.wsgi_request.user.inboxenprofile

    def test_backend_get_user_does_not_exist(self):
        backend = RateLimitWithSettings()
        user = backend.get_user(0)
        self.assertEqual(user, None)

    def test_backend_get_user_is_not_active(self):
        user = factories.UserFactory(is_active=False)
        backend = RateLimitWithSettings()
        backend_user = backend.get_user(user.id)
        self.assertEqual(backend_user, None)
